import copy

def my_map(l, *col):
    result = []
    length_col = []
    for i in col:
        length_col.append(len(i))
    for i in range(min(length_col)):
        if len(col) == 1:
            result.append(l(col[0][i]))
        else:
            el = []
            for c in col:
                el.append(c[i])
            result.append(l(tuple(el)))
    return result
	
def my_filter(l, col):
	result = []
	for i in col:
		if l(i):
			result.append(i)
	return result
	
def my_zip(*cols):
	min = 10000000000000000
	for col in cols:
		if len(col) < min:
			min = len(col)
	result = []
	for i in range(min):
		el = []
		for col in cols:
			el.append(col[i])
		result.append(el)
	return result
	
# testing collections
a = [1, 2, 3, 4]
b = [5, 6, 7, 8]
c = [9, 1, 2, 3]

# testing map
print("my_map")
print(my_map(lambda x: x*x, a))
print()

# testing filter
print("my_filter")
print(my_filter(lambda x: x % 2 == 0, a))
print()

# testing zip
print("my_zip")
print(my_zip(a, b, c))
print()