def my_zip(*args):
	return list(map(lambda *x: tuple(x), *args));
	
# testing collections
a = [1, 2, 3, 4]
b = [5, 6, 7, 8]
c = [9, 1, 2, 3]

print(my_zip(a, b, c))