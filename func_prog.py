def case1(l):
	return list(map(lambda x: x*2, l))
	
def case2(l1, l2, l3):
	return list(map(lambda x, y, z: x*y*z, l1, l2, l3))
	
def case3(l):
	return list(map(lambda x: len(x), l))
	
def case4(l):
	return list(filter(lambda x: l.index(x) % 2 == 0, l))
	
def case5(l):
	return list(filter(lambda x: x != "", l))
	
def case6(l1, l2, l3):
	return list(zip(l1, l2, l3))
	
def case7(l1, l2):
	return list(zip(l1, map(lambda x: x*2, l2)))
	
# testing collections
a = [1, 2, 3, 4]
b = [5, 6, 7, 8]
c = [9, 1, 2, 3]
d = ["hello", "WORLD", "NOT_WORLD", "HEY", "WHAT'S GOING ON?!", ""]

# testing case 1
print("Case 1:")
print(case1(a))
print()

# testing case 2
print("Case 2:")
print(case2(a, b, c))
print()

# testing case 3
print("Case 3:")
print(case3(d))
print()

# testing case 4
print("Case 4:")
print(case4(d))
print()

# testing case 5
print("Case 5:")
print(case5(d))
print()

# testing case 6
print("Case 6:")
print(case6(a, b, c))
print()

# testing case 7
print("Case 7:")
print(case7(a, b))
print()