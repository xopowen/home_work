a = float(input("Enter A: "))
b = float(input("Enter B: "))
c = float(input("Enter C: "))

if a == 0:
	# 3x-6=0
	print ("It's ", -c / b)
else:
	d = b**2 - 4*a*c
	if d < 0:
		print("There is no roots.")
	elif d == 0:
		sqr = -b / 2*a
		print("It's ", sqr)
	else:
		sqr1 = (-b - d**0.5) / 2*a
		sqr2 = (-b + d**0.5) / 2*a
		print("It's ", sqr1, " and ", sqr2)
	